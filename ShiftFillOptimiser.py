from ortools.linear_solver import pywraplp
from datetime import datetime, timedelta
import pytz


class ShiftFillOptimiser(object):
    # Prefixes

    def __init__(self, shifts, employees, rules, settings, days):
        self.shifts = shifts
        self.employees = employees
        self.rules = rules
        self.ruleIds = set(rules.keys())
        self.settings = settings
        self.days = days

        self.solver = pywraplp.Solver('ShiftFill', pywraplp.Solver.CBC_MIXED_INTEGER_PROGRAMMING)
        self.objective = self.solver.Objective()
        self.shiftEmpVariables = {}

        self.result = 0
        self.maxValue = 18446744073709551615
        self.minDate = datetime.now()
        self.maxDate = datetime.now()
        self.tz = pytz.timezone('Australia/Sydney')
        self.shiftPairs = {}
        self.assignedShiftPairs = {}

    def buildProblem(self):
        self.solver = pywraplp.Solver('ShiftFill', pywraplp.Solver.CBC_MIXED_INTEGER_PROGRAMMING)
        self.objective = self.solver.Objective()
        self.shiftEmpVariables = {}

        self.initVariables()
        self.initConstraints()
        self.initGoal()

    def initVariables(self):
        solver = self.solver
        print('start variables: {}'.format(datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S.%f')))

        for shift in self.shifts:
            for employee in self.employees:
                if shift.inSchedule(self.settings.start, self.settings.end):

                    varText = '{}-{}-{}-{}-{}'.format(employee.id, str(shift.start), str(shift.end), shift.departmentId, shift.id)

                    var = solver.LookupVariable(varText)
                    if shift.isFixed and shift.employeeId == employee.id:
                        if not var:
                            var = solver.IntVar(0, 1, varText)
                            var.SetLb(1)
                    elif not var and not shift.isFixed and shift.inSchedule(self.settings.start, self.settings.end) and employee.isEligible(shift):
                        var = solver.IntVar(0, 1, varText)

        print('end variables: {}'.format(datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S.%f')))


    def initConstraints(self):
        solver = self.solver
        print('start constraints: {}'.format(datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S.%f')))

        # Create a constraint for each shift
        for shift in self.shifts:
            if shift.inSchedule(self.settings.start, self.settings.end) and not shift.isFixed:
                # create constraints on a higher level
                constraintText = '{}-{}-{}-assignmentConstraint'.format(str(shift.start), str(shift.end), shift.id)
                c = solver.LookupConstraint(constraintText)
                if not c:
                    c = solver.Constraint(1, 1, constraintText)

                for employee in self.employees:
                    if employee.isEligible(shift):
                        var = solver.LookupVariable(
                            '{}-{}-{}-{}-{}'.format(employee.id, str(shift.start), str(shift.end), shift.departmentId,
                                                    shift.id))
                        c.SetCoefficient(var, 1)


        for i1, shift1 in enumerate(self.shifts):
            shift1.ignoreForFuture = False
            # reset the counter

        print('end constraints: {}'.format(datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S.%f')))

    def initGoal(self):
        solver = self.solver
        self.objective = solver.Objective()
        if self.settings.costObjective > 0:
            for shift in self.shifts:
                for employee in self.employees:
                    if shift.inSchedule(self.settings.start, self.settings.end) and employee.isEligible(shift) and not shift.isFixed:
                        varText = '{}-{}-{}-{}-{}'.format(employee.id, str(shift.start), str(shift.end), shift.departmentId, shift.id)

                        var = solver.LookupVariable(varText)
                        if var:
                            self.objective.SetCoefficient(var, round(employee.hourlyRate / 60 * shift.payDuration * self.settings.costObjective, 2))

    def initRules(self):
        print('start rules: {}'.format(datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S.%f')))
        solver = self.solver

        # Always allow for slack
        for shift in self.shifts:
            if shift.inSchedule(self.settings.start, self.settings.end) and not shift.isFixed:
                constraintText = '{}-{}-{}-assignmentConstraint'.format(str(shift.start), str(shift.end), shift.id)
                c = solver.LookupConstraint(constraintText)
                if c:
                    varText = '{}-{}-{}-s'.format(str(shift.start), str(shift.end), shift.id)
                    slackVar = solver.LookupVariable(varText)
                    if not slackVar:
                        slackVar = solver.IntVar(0, 1, varText)
                        c.SetCoefficient(slackVar, 1)

        # Rule 1: Penalty for keeping unassigned shifts
        if "1" in self.ruleIds:
            print("starting rule 1")
            rule1s = self.rules["1"]
            for rule1 in rule1s:
                if rule1.penalty > 0:
                    for shift in self.shifts:
                        if shift.inSchedule(self.settings.start, self.settings.end) and (
                                not rule1.departmentIds or shift.departmentId in rule1.departmentIds) and (
                                not shift.isFixed or (shift.isFixed and rule1.considerFixedShifts)):
                            slackVar = solver.LookupVariable(
                                '{}-{}-{}-s'.format(str(shift.start), str(shift.end), shift.id))
                            if rule1.isMandatory:
                                slackVar.SetUb(0)
                            self.objective.SetCoefficient(slackVar, rule1.penalty * self.settings.ruleObjective)

        ###############################################################################
        #####         TO DO add a maximum consecutive shift type constraint       #####
        ###############################################################################

        # Rule 2: Maximum N consecutive days working shift type X
        
        # Assuming that Ids define the order of the shifts

        if "2" in self.ruleIds:
            print("starting rule 2")
            rule2s = self.rules["2"]
            X = 2 # Assuming there are two types of shifts, though X can be changed accordingly        
            N = 3 # Assuming maximum consecutive shifts is 3, though N can be changed accordingly

                
            for rule2 in rule2s:
                if rule2.penalty > 0:
                    for x in range(1, X + 1): # Loop over the types to check for violations of constraint seperately
                        shiftsPerEmployee = {} # Dictionary to group shifts per Employee
                        for employee in self.employees: # Create structure of dictionary
                            shiftsPerEmployee[employee] = []
                                                       
                        for shift in self.shifts:
                            if shift.type == x and shift.employeeId != None: # Only consider shifts of type x                               
                                shiftsPerEmployee[shift.employeeId].append(shift.id)
                        
                        penalty = 0 # Set initial value for penalty
                        
                        # If the differences between N + 1 shiftIds for an employee is 1, we have a violation
                        # Example Ids 19 - 20 - 21 - 22 => differences are 1 - 1 - 1 => violation
                        # Example Ids 18 - 20 - 21 - 22 => differences are 2 - 1 - 1 => no violation
                        
                        for emp, shiftsIds in shiftsPerEmployee.items():
                            shiftIdsDifferences = [shiftsIds[i + 1] - shiftsIds[i] for i in range(len(shiftsIds) - 1)] 
                            # Compute differences in shiftIds
                            for i in range(len(shiftIdsDifferences) - N):
                                # Check if i == 1 and i + 1 == 1 and ... and i + (N - 1) == 1
                                consecutiveShifts = True # Default true
                                for n in range(N):
                                    consecutiveShifts *= (shiftIdsDifferences[i + n] == 1) # bool becomes false when any condition is false
                                if consecutiveShifts: # if condition is true, violation and add one to the penalty
                                    penalty += 1
                    
                    
                    # TO DO: Add penalty variables per employee to the model and objective.
                    
                    # self.objective.SetCoefficient(slackVar, rule2.penalty * self.settings.ruleObjective)


        print('end rules: {}'.format(datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S.%f')))


    def solveProblem(self):

        print('start solve problem: {}'.format(datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S.%f')))
        before = datetime.now()
        solver = self.solver

        timeLimitSec = self.settings.runtime
        solver.SetTimeLimit(timeLimitSec * 1000)

        print('# Variables: {}'.format(solver.NumVariables()))
        print('# Constraints: {}'.format(solver.NumConstraints()))

        self.result = solver.Solve()

        after = datetime.now()

        print('end solve problem: {}'.format(datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S.%f')))
        print('RESULT: {} [{}]'.format(self.result, pywraplp.Solver.OPTIMAL))
        print('Problem solved in {} seconds'.format((after - before).total_seconds()))
        print('Optimal objective value: {}'.format(solver.Objective().Value()))

    def getOutputResults(self, fileHandler):
        returnData = {}
        solver = self.solver

        if self.result == solver.INFEASIBLE:
            returnData['result'] = 0
        elif self.result == solver.OPTIMAL:
            returnData['result'] = 1
        elif self.result == solver.NOT_SOLVED:
            returnData['result'] = 2
        else:
            returnData['result'] = None

        returnData['goal_score'] = round(solver.Objective().Value(), 2)
        for shift in self.shifts:
            if shift.isFixed:
                for employee in self.employees:
                    if shift.employeeId == employee.id:
                        returnData[
                            'goal_score'] = solver.Objective().Value()

        returnData['shifts'] = []

        shiftsData = {}

        for shift in self.shifts:
            if shift.inSchedule(self.settings.start, self.settings.end):
                if shift.isFixed:

                    shiftData = {}
                    shiftData['shift_id'] = shift.id
                    shiftData['user_id'] = shift.employeeId
                    shiftData['department_id'] = shift.departmentId
                    shiftData['start'] = shift.start
                    shiftData['finish'] = shift.end
                    shiftData['type'] = shift.type
                    shiftData['is_fixed'] = shift.isFixed

                    # shiftData['subshifts'] = shift.subShifts
                    shiftData['violation_costs'] = 0
                    if len(shift.breaks) > 0:
                        shiftData['breaks'] = []
                        for b in shift.breaks:
                            shiftData['breaks'].append({
                                "start": b.start,
                                "finish": b.end
                            })
                    if len(shift.shiftLicenses) > 0:
                        shiftData['licenses'] = []
                        for l in shift.shiftLicenses:
                            shiftData['licenses'].append({
                                "id": l.id
                            })

                    shiftsData[shift.id] = shiftData

                else:

                    for employee in self.employees:
                        if employee.isEligible(shift):
                            shiftText = '{}-{}-{}'.format(str(shift.start), str(shift.end), shift.id)
                            varText = '{}-{}-{}-{}-{}'.format(employee.id, str(shift.start), str(shift.end),
                                                              shift.departmentId, shift.id)
                            shiftEmpVariable = self.solver.LookupVariable(varText)
                            if shiftEmpVariable and shiftEmpVariable.solution_value() > 0:
                                shiftData = {}
                                shiftData['shift_id'] = shift.id
                                shiftData['violation_costs'] = 0
                                shiftData['user_id'] = employee.id
                                shiftData['department_id'] = shift.departmentId
                                shiftData['start'] = shift.start
                                shiftData['finish'] = shift.end
                                shiftData['type'] = shift.type
                                shiftData['is_fixed'] = shift.isFixed

                                # shiftData['subshifts'] = shift.subShifts
                                if len(shift.breaks) > 0:
                                    shiftData['breaks'] = []
                                    for b in shift.breaks:
                                        shiftData['breaks'].append({
                                            "start": b.start,
                                            "finish": b.end
                                        })
                                if len(shift.shiftLicenses) > 0:
                                    shiftData['licenses'] = []
                                    for l in shift.shiftLicenses:
                                        shiftData['licenses'].append({
                                            "id": l.id
                                        })
                                shiftData['shift_costs'] = round(
                                    shift.payDuration * (employee.hourlyRate / 60) * self.settings.costObjective, 2)
                                shiftsData[shift.id] = shiftData

        returnData['rule_violations'] = []

        #Check for rule violations

        #Rule 1 no unassigned shifts
        if "1" in self.ruleIds:
            for shift in self.shifts:
                if not shift.isFixed:
                    slackVarText = '{}-{}-{}-s'.format(str(shift.start), str(shift.end), shift.id)
                    unassignedShiftSlackVar = solver.LookupVariable(slackVarText)
                    if unassignedShiftSlackVar and unassignedShiftSlackVar.solution_value() > 0:
                        unassignedShiftCoeff = self.objective.GetCoefficient(unassignedShiftSlackVar)
                        if unassignedShiftCoeff > 0:
                            if shift.id not in shiftsData:
                                returnData['rule_violations'].append(
                                    self.violationData('1', shift.id, unassignedShiftCoeff, None, shift.start,
                                                       shift.start, shift.end, None, shift.type))


        ###############################################################################
        #####         TO DO add a rule violation check for rule 2                 #####
        ###############################################################################

        # Rule 2: Maximum N consecutive days working shift type X
        
        if "2" in self.ruleIds:
            for shift in self.shifts:
                if not shift.isFixed:                   
                    # consecutiveShiftPenalty = 0
                    
                    # We should have something similar here as in the initRules for rule 2, that gets the penalty based on
                    # how often rule 2 is violated...
                    
                    # if consecutiveShiftSlackVar and consecutiveShiftSlackVar.solution_value() > 0:
                    #    unassignedShiftCoeff = self.objective.GetCoefficient(consecutiveShiftSlackVar)
                    #    if unassignedShiftCoeff > 0:
                    #        if shift.id not in shiftsData:
                    #            returnData['rule_violations'].append(
                    #               self.violationData('2', shift.id, unassignedShiftCoeff, None, shift.start,
                    #                                 shift.start, shift.end, None, shift.type))


    def violationData(self, ruleId, shiftId, violationCosts, employeeId, date, shiftStart, shiftEnd, departmentId,
                      shiftType):
        violationData = {}
        violationData['rule_id'] = ruleId
        if shiftId:
            violationData['shift_id'] = shiftId
        violationData['violation_costs'] = violationCosts
        if employeeId:
            violationData['user_id'] = employeeId
        if date:
            violationData['date'] = date
        if shiftStart:
            violationData['shift_start'] = shiftStart
        if shiftEnd:
            violationData['shift_finish'] = shiftEnd
        if departmentId:
            violationData['role'] = departmentId
        if shiftType:
            violationData['shift_type'] = shiftType
        return violationData

    # Returns the shifts on the day - either by the shift start time or by most of the shift hours being worked on the day
    def getShiftsToday(self, day):
        shiftsToday = []
        shiftsToday = [shift for shift in self.shifts if shift.start >= day.date and shift.start < day.date + int(timedelta(days=1).total_seconds())]
        return shiftsToday
